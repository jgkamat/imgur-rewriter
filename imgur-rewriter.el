;;; imgur-rewriter.el --- A package for rewriting imgur URLs -*- lexical-binding: t -*-

;; Copyright (C) 2018 Jay Kamat
;; Author: Jay Kamat <jaygkamat@gmail.com>
;; Version: 0.0.1
;; Keywords: multimedia, hypermedia, comm
;; URL: http://gitlab.com/jgkamat/imgur-rewriter
;; Package-Requires: ((emacs "25.0") (s "1.12"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Unfortunately, most of the image URLs sent on the internet are from imgur. Those that don't use imgur are usually
;; decent enough to send direct links. Even more unfortunately, those who send from imgur usually do not send direct
;; links. Recently, website links do not display an image without js enabled, which is a security and privacy concern.
;;
;; This package tries to provide a facility to translate imgur links into their direct counterparts.
;;
;; Methodology taken from
;; https://raw.githubusercontent.com/alexgisby/imgur-album-downloader/master/imguralbum.py
;;
;;; Examples:
;; (imgur-rewriter-rewrite-string "hello everyone, look at this: https://imgur.com/gallery/Nvx8foP bye!" #'print)
;; (imgur-rewriter-rewrite-url "https://imgur.com/gallery/Nvx8foP" #'print)
;;
;;;; Function to rewrite clicked links (and open images for galleries):
;; (defun jay/imgur-override (func external url &rest _)
;;   "Rewrite imgur URLs before they hit the browser."
;;   (imgur-rewriter-rewrite-url
;;    url
;;    (lambda (urls)
;;      (dolist (url (reverse urls))
;;        (apply func external url _)))))
;; ;; Use your own function here for browse-url
;; (advice-add #'browse-url-generic :around #'jay/imgur-override))



;;; Code:

(require 'rx)
(require 'subr-x)
(require 'cl-lib)
(require 'cl-macs)
(require 'url)
(require 's)

(defun imgur-rewriter--imgur-p (url &optional pos)
  "Return non-nil if URL is an imgur link, searching from POS.

Also sets `string-match' groups to the protocol and ID contained in the link."
  (string-match
   (rx (group-n 1 "http" (optional "s"))
       "://" (optional "www.") (optional "m.") "imgur.com/"
       (optional (or "a" "gallery") "/")
       (group-n 2 (1+ (any alnum)))
       (optional "#" (1+ (any digit))))
   url pos))

(defun imgur-rewriter-rewrite-url (url callback)
  "Translate a URL from imgur -> direct link.

Calls CALLBACK with a list of direct link URLs, or a list of the
passed URL if nothing is found.

Return nil if no link was found."
  ;; Check if this is a imgur link
  (if-let
      ((match-pos (imgur-rewriter--imgur-p url))
       (protocol (match-string 1 url))
       (ident (match-string 2 url))
       (blog-url (concat "http://imgur.com/a/" ident "/layout/blog"))
       (url-request-method "GET"))
      (url-retrieve blog-url (apply-partially #'imgur-rewriter--handle-response callback ident))
    (funcall callback (list url))))

(cl-defun imgur-rewriter-rewrite-string
    (str callback &optional (start 0))
  "Rewrite any imgur URLs in STR, replacing them.

Calls CALLBACK with a new string."
  (if (not (imgur-rewriter--imgur-p str start))
      (funcall callback str)
    (let ((beg (match-beginning 0))
          (end (match-end 0)))
      (imgur-rewriter-rewrite-url
       (substring str beg end)
       (lambda (urls)
         (let* ((result (string-join urls ", "))
                (str-tail (+ beg (length result))))
           (imgur-rewriter-rewrite-string
            (concat (substring str 0 beg)
                    result
                    (substring str end))
            callback
            str-tail)))))))

(defun imgur-rewriter--handle-response (callback ident ret)
  (let ((b (current-buffer))
        (err (plist-get ret :error)))
    ;; Error handling
    (unwind-protect
        (if err
            ;; We probably got a 404, let's bail and assume this is a normal image
            (funcall callback
                     (list
                      (concat "http://i.imgur.com/" ident ".png")))
          (let ((image-parts
                 (cl-remove-duplicates
                  (mapcar #'cl-rest
                          (s-match-strings-all
                           (rx "{\"hash\":\"" (group (1+ alnum)) "\""
                               (*? any)
                               "\"ext\":\"" (group "." (1+ alnum)) "\"")
                           (buffer-string)))
                  :test #'equal)))
            (funcall callback
                     (mapcar (lambda (x)
                               (concat "http://i.imgur.com/"
                                       (cl-first x)
                                       (cl-second x)))
                             image-parts))))
      (when (buffer-live-p b)
        (kill-buffer b)))))



(provide 'imgur-rewriter)

;;; imgur-rewriter.el ends here.
